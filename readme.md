# Start

Make sure DOCKER is installed and docker (default) virtual machine is started.

1. Build the image with custom PHP rules: `docker build -t robertosnap/wordpress .`
2. Start wordpress and database run `docker-compose up -d`

Pause `docker-compose down`

Destroy `docker-compose down --volumes`

# Links

Taken from here https://docs.docker.com/compose/wordpress
